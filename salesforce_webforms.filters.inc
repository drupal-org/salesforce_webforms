<?php

/**
 * @file
 * Data conversion filters.
 */

/**
 * Converts a webform date field into Salesforce format.
 *
 * @param string $date
 *   A date to be converted.
 *
 * @return string
 *   The filtered value in the form YYYY-MM-DD
 */
function salesforce_webforms_date_filter($date) {
  // Get the default date format.
  $format = webform_date_format();
  $date = date_parse_from_format($format, $date);
  return sprintf('%04.4d-%02.2d-%02.2d', $date['year'], $date['month'], $date['day']);
}

/**
 * Converts a date/time field into Salesforce format.
 *
 * @param string $datetime
 *   A date to be converted.
 *
 * @return string
 *   The filtered value in the form YYYY-MM-DDTHH:MM:SSZ
 */
function salesforce_webforms_datetime_filter($datetime) {
  $format = webform_date_format();
  $date = date_parse_from_format($format, $datetime);
  return sprintf('%04.4d-%02.2d-%02.2dT%02.2d:%02.2d:%02.2dZ',
    $date['year'], $date['month'], $date['day'], $date['hour'], $date['minute'],
     $date['second']);
}

// Defines a replacement for date_parse_from_format() if it is not defined.
if (!function_exists('date_parse_from_format')) {

  /**
   * Parses a date based on a date format.
   *
   * @param string $format
   *   Format accepted by DateTime::createFromFormat().
   * @param string $date
   *   Date in the format specified by $format.
   *
   * @return array
   *   Associative array as documented at
   * @link http://php.net/manual/en/function.date-parse-from-format.php php.net @endlink
   */
  function date_parse_from_format($format, $date) {
    // Reverse engineer date formats.
    $keys = array(
      'Y' => array('year', '\d{4}'),
      'y' => array('year', '\d{2}'),
      'm' => array('month', '\d{2}'),
      'n' => array('month', '\d{1,2}'),
      'M' => array('month', '[A-Z][a-z]{3}'),
      'F' => array('month', '[A-Z][a-z]{2,8}'),
      'd' => array('day', '\d{2}'),
      'j' => array('day', '\d{1,2}'),
      'D' => array('day', '[A-Z][a-z]{2}'),
      'l' => array('day', '[A-Z][a-z]{6,9}'),
      'u' => array('hour', '\d{1,6}'),
      'h' => array('hour', '\d{2}'),
      'H' => array('hour', '\d{2}'),
      'g' => array('hour', '\d{1,2}'),
      'G' => array('hour', '\d{1,2}'),
      'i' => array('minute', '\d{2}'),
      's' => array('second', '\d{2}'),
    );

    // Convert format string to regex.
    $regex = '';
    $chars = str_split($format);
    foreach ($chars as $n => $char) {
      $last_char = isset($chars[$n - 1]) ? $chars[$n - 1] : '';
      $skip_current = '\\' == $last_char;
      if (!$skip_current && isset($keys[$char])) {
        $regex .= '(?P<' . $keys[$char][0] . '>' . $keys[$char][1] . ')';
      }
      elseif ('\\' == $char) {
        $regex .= $char;
      }
      else {
        $regex .= preg_quote($char);
      }
    }

    $dt = array();
    $dt['error_count'] = 0;
    // Now try to match it.
    if (preg_match('#^' . $regex . '$#', $date, $dt)) {
      foreach ($dt as $k => $v) {
        if (is_int($k)) {
          unset($dt[$k]);
        }
      }
      if (!checkdate($dt['month'], $dt['day'], $dt['year'])) {
        $dt['error_count'] = 1;
      }
    }
    else {
      $dt['error_count'] = 1;
    }
    $dt['errors'] = array();
    $dt['fraction'] = '';
    $dt['warning_count'] = 0;
    $dt['warnings'] = array();
    $dt['is_localtime'] = 0;
    $dt['zone_type'] = 0;
    $dt['zone'] = 0;
    $dt['is_dst'] = '';
    return $dt;
  }
}
